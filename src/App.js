import style from './App.module.css';
import Header from './Header';
import SensorList from './routes/SensorList';
import SensorInfo from './routes/SensorInfo';
import React, {useState} from 'react';
import { Route, Routes, useNavigate} from 'react-router-dom';

function App() {
  const navigate = useNavigate();
  const [sensorData, setSensorData] = useState(null);
  const [url, setUrl] = useState(null);
  return (
    <div className={style.App}>
      <Header className={style.header} onChange={(evt) => setUrl(evt.target.value)} onClick={tryLoadData} />
      <Routes>
        <Route 
          index
          element={
            <main className={style.center}>
              <p>Please enter the data url</p>
            </main>
          } 
        />
        <Route path='dataError' element={<main className={style.center}><span >Error while getting data from the url</span></main>}/>
        <Route path='view' element={<SensorList sensorData={sensorData}/>}>
          <Route path=':sensorName' element={<SensorInfo sensorData={sensorData} />} />
        </Route>
        <Route
          path="*"
          element={
            <main className={style.center}>
              <p>There's nothing here!</p>
            </main>
          }
        />
      </Routes>
    </div>
  );

  async function tryLoadData() {
    try {
      const toFetchUrl = url.includes('/') ? url : '/' + url // If the file is loaded from the server, take path from root and not current location
      setSensorData(await loadData(toFetchUrl))
      navigate('/view')
    }catch(e) {
      navigate('/dataError')
      console.error(e)
    }
  }
}

async function loadData(url) {
  const response = await request('GET', url);
  return JSON.parse(response);
}

async function request(method, url) {
  return new Promise((resolve, reject) => {
    // Sends a request
    const oReq = new XMLHttpRequest();
    oReq.open(method, url, true);
    oReq.onload = () => {
      if(oReq.status !== 200) reject(`Error while fetching data from the url: ${url}\nReceived status: ${oReq.status} - ${oReq.statusText}`);
      resolve(oReq.responseText) 
    };
    oReq.send();
  })
}

export default App;

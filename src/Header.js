import style from './Header.module.css';

function Header(props) {
    return (
        <div className={props.className}>
            <input 
                className={style.urlInput} type="text"
                placeholder="Enter url to sensor data"
                onChange={props.onChange} />
            <button className={style.btnLoad} onClick={props.onClick}>
                <span>Load Data!</span>
            </button>
        </div>
    );
}

export default Header;
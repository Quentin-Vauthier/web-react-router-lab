import style from './SensorButton.module.css';

export default function SensorButton(props) {
    return (<div className={`${style.sensorButton} ${style.fadeIn}`}>
        <span>{props.sensor.name}</span>
        <br/>
        <span>Type: {props.sensor.type[0] + props.sensor.type.substr(1).toLowerCase()}</span>
        <br/>
    </div>)
}
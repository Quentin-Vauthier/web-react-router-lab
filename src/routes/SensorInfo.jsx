import { useParams } from 'react-router-dom';
import style from './SensorInfo.module.css';
import globalStyle from '../App.module.css';
import SensorData from './SensorData.jsx';


export default function SensorInfo(props) {
    const params = useParams();
    let sensor = props.sensorData.find(
      (o) =>
        o.name
          .toLowerCase()
          .replace(/ /g, "_")
          .normalize("NFD")
          .replace(/[\u0300-\u036f]/g, "") === params.sensorName
    );
    if(!sensor) return (<div className={globalStyle.center}><span>Unable to find sensor data</span></div>);
    return (
      <div className={style.sensorInfo}>
        <div className={style.sensorName}>
          <span>{sensor.name}</span>
        </div>
        <br />
        <SensorData sensor={sensor}/>
      </div>
    );
}
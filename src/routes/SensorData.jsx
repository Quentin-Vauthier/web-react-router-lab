import style from './SensorData.module.css';
import globalStyle from '../App.module.css';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import { Chart, CategoryScale, LinearScale, PointElement, LineElement } from "chart.js";

export default function SensorData(props) {
    const sensor = props.sensor;
    if(!sensor.data) return (<div className={globalStyle.center}><span>No data for this sensor</span></div>)
    return generateData(sensor);
}

function generateData(sensor) {
    const data = sensor.data;
    const type = sensor.type;
    switch(type) {
        case 'DOOR':
            return generateDataForDoor(sensor);
        case 'TEMPERATURE': {
          const lastLabel = sensor.data.labels[sensor.data.labels.length-1];
            const lastValue = sensor.data.values[sensor.data.values.length-1];
            const lastDate  = moment(lastLabel);
            return (
              <div className={style.sensorData}>
                <span className={style.lastData}>Last recorded temperature {lastDate.isValid() ? `(${lastDate.format("l LT")})` : ''} : {lastValue}°{sensor.unit || 'C'}</span>
                {generateChart(sensor, 12)}
              </div>
            );
        }
        case 'FAN_SPEED': {
          const lastLabel = sensor.data.labels[sensor.data.labels.length-1];
          const lastValue = sensor.data.values[sensor.data.values.length-1];
          const lastDate  = moment(lastLabel);
          return (
            <div className={style.sensorData}>
              <span className={style.lastData}>Last recorded speed {lastDate.isValid() ? `(${lastDate.format("l LT")})` : ''} : {lastValue}RPM</span>
              {generateChart(sensor, 20)}
            </div>
          );
        }
        default: return (<pre>{JSON.stringify(data, null, 4)}</pre>);
    }
}

function generateDataForDoor(sensor) {
  const isOpened = sensor.data.value === 1;
  const imgSrc = isOpened ? '/openingDoor.gif' : '/closingDoor.gif';
  const imgAlt = isOpened ? 'Door opening' : 'Door closing';
  const textState = isOpened ? 'opened' : 'closed';
  return (
    <div className={style.doorData}>
      <span className={style.doorCaption}>The door is {textState}</span>
      <br/>
      <img className={style.doorImg} alt={imgAlt} src={imgSrc}/>
    </div>
  )
}

function generateChart(sensor, nbOfValues) {
    Chart.register(CategoryScale, LinearScale, PointElement, LineElement);
    return (
      <div className={style.chart}>
      <Line
        data={{
          labels: getChartLabelsFromTS(sensor.data.labels, nbOfValues),
          datasets: [
            {
              label: sensor.name,
              data: sensor.data.values.slice(-nbOfValues),
              fill: false,
              borderColor: '#AF40FF',
              tension: 0.2
            },
          ]
        }}
        options={{
          responsive: true,
          maintainAspectRatio: false
        }}
      />
      </div>
    );
}

// Returns chart data from time series
function getChartLabelsFromTS(labels, nbOfValues) {
  const labeledDays = [];
  return labels.map((s) => {
    const m = moment(s);
    const day = m.format("L");
    const hourMinute = m.format("HH:mm");
    if(!labeledDays.includes(day)) {
      labeledDays.push(day);
      return [hourMinute, day];
    }
    return hourMinute;
  })
}
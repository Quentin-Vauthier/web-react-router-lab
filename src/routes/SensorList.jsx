import style from './SensorList.module.css';
import globalStyle from '../App.module.css';
import { NavLink, Outlet } from 'react-router-dom';
import SensorButton from '../SensorButton'
export default function SensorList(props) {
    if(!props.sensorData || props.sensorData.length === 0) {
        return (<span className={globalStyle.center}>Data not found</span>);
    }
    return (
      <div className={style.flex}>
        <div className={style.sensorList}>
          {props.sensorData.map((s) => (
            <NavLink
              className={({ isActive }) => (isActive ? style.active : "")}
              to={`${s.name
                .toLowerCase()
                .replace(/ /g, "_")
                .normalize("NFD")
                .replace(/[\u0300-\u036f]/g, "")}`}
              key={s.id}
            >
              <SensorButton sensor={s} />
            </NavLink>
          ))}
        </div>
        <Outlet />
      </div>
    );
}